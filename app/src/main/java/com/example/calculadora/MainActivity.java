package com.example.calculadora;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Button;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    String numerogeral;
    boolean modoresultado = false;
    String expressao;
    int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void adicionarNumero(View view) {
        String numero = ((TextView) view).getText().toString();
        TextView resultado = ((TextView) findViewById(R.id.resultado));
        String result = (String) resultado.getText();
        if(numero.equals(".")){
            if(result.contains(".")){

            } else {
                resultado.setText(resultado.getText() + numero);
            }
        } else {
            resultado.setText(resultado.getText() + numero);
        }
    }

    public void expressao(View view){
        if(this.modoresultado == false){
            String expressao = ((TextView) view).getText().toString();
            TextView historico = ((TextView) findViewById(R.id.historico));
            TextView resultado = ((TextView) findViewById(R.id.resultado));
            if(resultado.getText() == ""){
                if(expressao.equals("-")){
                    resultado.setText(expressao);
                }
            } else {
                if(resultado.getText().equals("-")){

                } else if(historico.getText() == ""){
                    this.numerogeral = (String) resultado.getText();
                    this.expressao = expressao;
                    historico.setText(resultado.getText() + " " + expressao);
                    resultado.setText("");
                }
            }
        }

    }

    public void resultado(View view){
        TextView historico = ((TextView) findViewById(R.id.historico));
        if(historico.getText() != ""){
            double soma = 0;
            TextView resultado = ((TextView) findViewById(R.id.resultado));

            double valor = Double.parseDouble((String) resultado.getText());
            if(this.expressao.equals("+")){
                soma = Double.parseDouble(this.numerogeral) + valor;
                resultado.setText(Double.toString(soma));
                historico.setText("");
            } else if(this.expressao.equals("-")){
                soma = Double.parseDouble(this.numerogeral) - valor;
                resultado.setText(Double.toString(soma));
                historico.setText("");
            } else if(this.expressao.equals("X")){
                soma = Double.parseDouble(this.numerogeral) * valor;
                resultado.setText(Double.toString(soma));
                historico.setText("");
            } else if(this.expressao.equals("/")){
                soma = Double.parseDouble(this.numerogeral) / valor;
                resultado.setText(Double.toString(soma));
                historico.setText("");
            }
        }
    }

    public void limpar(View view){
        TextView resultado = ((TextView) findViewById(R.id.resultado));
        TextView historico = ((TextView) findViewById(R.id.historico));
        historico.setText("");
        resultado.setText("");
        this.modoresultado = false;
    }

}